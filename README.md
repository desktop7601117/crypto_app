# Crypto Account Verification Desktop Application

## 🪧 About

Creation of a fictional application for hacking cryptocurrency accounts, with the aim of understanding blockchain as well as discovering the Electron desktop application compilation tool. Additionally, a similar project under Node.js is currently under development but not yet functional.

## 📚 Table of Contents

- 🪧 [About](#about)
- 🚀 [Installation](#installation)
- 🤝 [Contribution](#contribution)
- 🏗️ [Languages & Frameworks](#languages--frameworks)

## 🚀 Installation

### 📂 Downloading the Folder
  - Retrieve the zip folder from GitHub.
  - Create a new folder with the zip file inside.
  - Unzip the file and open the new folder in your code editor.

### 💻 npm Command
  - Once in your code editor, open the project folder and your terminal to navigate to the electron folder with the command `$ cd electron`.
  - Then, execute the command `$ npm install` to install project dependencies.
  - Once this step is completed, you can run the application. For debug mode, use `$ npm run dev`, otherwise use `$ npm start`.

### 🍏 Mac Application
  - For Mac users, execute the command `$ npm run build:mac`.
  - A dist folder is created inside which contains a mac-arm64 folder with the crypto-app.app application inside.

### 🖥️ Windows Application
  - For Windows users, execute the command `$ npm run build:win`.
  - A dist folder is created inside which contains a win-arm64-unpacked folder with the crypto-app.exe application inside.

### 🐧 Linux Application
  - For Linux users, execute the command `$ npm run build:linux`.
  - A dist folder is created inside which contains a linux-arm64-unpacked folder with the crypto-app application inside.

## 🤝 Contribution

Developed by [*Mathis*](https://github.com/mathis1009), [*Sofian*](https://github.com/sofian-bali), and [*Maxime*](https://github.com/MaximeLemesle)

## 🛠️ Languages & Frameworks

<img src="https://img.shields.io/badge/Tools-Electron-47848F?style=flat&logo=electron&logoColor=white" />
<img src="https://img.shields.io/badge/Code-JavaScript-yellow?style=flat&logo=javascript&logoColor=white" />
<img src="https://img.shields.io/badge/Code-Node.js-darkgreen?style=flat&logo=node.js&logoColor=white" />
<img src="https://img.shields.io/badge/Style-CSS-blue?style=flat&logo=css3&logoColor=white" />
